package com.ming.user.feign.fallback;

import com.ming.common.entity.ResponseCode;
import com.ming.common.entity.ResultData;
import com.ming.user.feign.UserFeign;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @Classname UserFeignFallbackImpl
 * @Description TODO
 * @Date 2021/1/6 11:15
 * @Created by yanming.fu
 */
@Slf4j
@Component
public class UserFeignFallbackImpl implements UserFeign {

    @Setter
    private Throwable cause;

    @Override
    public ResultData echo(String id) {
        String ErrorMsg="feign 查询system信息失败："+id;
        //log.error(ErrorMsg, id, cause);
        return ResultData.fail(ResponseCode.ERROR.val(), ErrorMsg);
    }
}

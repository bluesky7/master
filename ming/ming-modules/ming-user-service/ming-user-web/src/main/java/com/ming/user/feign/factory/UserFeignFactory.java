package com.ming.user.feign.factory;

import com.ming.user.feign.UserFeign;
import com.ming.user.feign.fallback.UserFeignFallbackImpl;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @Classname UserFeignFactory
 * @Description TODO
 * @Date 2021/1/6 10:11
 * @Created by yanming.fu
 */
@Component
public class UserFeignFactory implements FallbackFactory<UserFeign> {

    @Override
    public UserFeign create(Throwable throwable) {
        UserFeignFallbackImpl remoteUserServiceFallback = new UserFeignFallbackImpl();
        remoteUserServiceFallback.setCause(throwable);
        return remoteUserServiceFallback;
    }
}

package com.ming.user.feign;

import com.ming.user.JwtToken;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Description 调授权服务  进行授权
 * @return
 * @date 2021/2/3 17:07
 * @auther yanming.fu
 */
@FeignClient(value = "ming-authorization")
public interface Oauth2Feign {

    @PostMapping("/oauth/token")
    ResponseEntity<JwtToken> getToken(
            @RequestParam("grant_type") String grantType, // 授权类型
            @RequestParam("username") String username, // 用户名
            @RequestParam("password") String password, // 用户的密码
            @RequestParam("login_type") String loginType,  // 登录的类型
            @RequestHeader("Authorization") String basicToken // Basic Y29pbi1hcGk6Y29pbi1zZWNyZXQ=
    ) ;
}

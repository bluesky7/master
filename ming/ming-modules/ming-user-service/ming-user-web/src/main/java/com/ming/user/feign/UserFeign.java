package com.ming.user.feign;

import com.ming.common.entity.ResultData;
import com.ming.user.feign.factory.UserFeignFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @Classname UserFeign
 * @Description contextId在Feign Client的作用是在注册Feign Client Configuration的时候需要一个名称，名称是通过getClientName方法获取的
 * @Date 2021/1/6 10:10
 * @Created by yanming.fu
 */
//@FeignClient(serviceId = "ming-system",decode404=true,fallbackFactory = UserFeignFactory.class,contextId= "mingSystem")
@FeignClient(serviceId = "ming-system")
public interface UserFeign {

    @RequestMapping(value = "/system/echo/{str}", method = RequestMethod.GET)
    ResultData echo(@PathVariable("str") String str);
}

package com.ming.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

/**
    * 权限配置
    */
@ApiModel(value="系统权限")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "sys_privilege")
public class SysPrivilege {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY,generator = "JDBC")
    @ApiModelProperty(value="主键")
    private Long id;

    /**
     * 所属菜单Id
     */
    @Column(name = "menu_id")
    @ApiModelProperty(value="所属菜单Id")
    private Long menuId;

    /**
     * 功能点名称
     */
    @Column(name = "name")
    @ApiModelProperty(value="功能点名称")
    private String name;

    /**
     * 功能描述
     */
    @Column(name = "description")
    @ApiModelProperty(value="功能描述")
    private String description;

    @Column(name = "url")
    @ApiModelProperty(value="")
    private String url;

    @Column(name = "method")
    @ApiModelProperty(value="")
    private String method;

    /**
     * 创建人
     */
    @Column(name = "create_by")
    @ApiModelProperty(value="创建人")
    private Long createBy;

    /**
     * 修改人
     */
    @Column(name = "modify_by")
    @ApiModelProperty(value="修改人")
    private Long modifyBy;

    /**
     * 创建时间
     */
    @Column(name = "created")
    @ApiModelProperty(value="创建时间")
    private Date created;

    /**
     * 修改时间
     */
    @Column(name = "last_update_time")
    @ApiModelProperty(value="修改时间")
    private Date lastUpdateTime;

    /**
     * 标记当前的角色是否有该权限
     */
    @ApiModelProperty(value="当前角色是否拥有这个权限")
    private int own ;
}
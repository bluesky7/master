package com.ming.authorization;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Classname AuthApplication
 * @Description 鉴权服务启动类
 * @Date 2021/2/2 15:20
 * @Created by yanming.fu
 */
@SpringBootApplication
public class AuthApplication {
    private static final Logger logger = LoggerFactory.getLogger(AuthApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(AuthApplication.class, args);
        String msg = String.format("%s启动完毕", "【鉴权服务】");
        System.out.println(msg);
        logger.info("========================【鉴权服务】启动完毕========================");
    }
}

package com.ming.mq.common;

import com.ming.mq.common.config.MingMqConfig;
import com.ming.mq.common.config.MqImportSelector;
import com.ming.mq.common.config.TestConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import javax.annotation.Resource;

/**
 * @Classname EnableMqAutoConfig
 * @Description TODO
 * @Date 2021/1/13 11:01
 * @Created by yanming.fu
 */
@Configuration
@Import(MqImportSelector.class)
public class EnableMqAutoConfig {

    @Resource(name="mingMqConfig2")
    private MingMqConfig mingMqConfig2;
  /*  @Resource
    private com.ming.mq.common.config.TestConfig testConfig;*/

    @Bean
    public String getStr(){
       /* int i = testConfig.hashCode();*/
        return mingMqConfig2.getAccessKey();

    }
}

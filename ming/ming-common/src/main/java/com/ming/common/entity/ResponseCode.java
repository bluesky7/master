package com.ming.common.entity;

/**
 * @Classname ResponseCode
 * @Description code码封装枚举类
 * @Date 2021/1/6 11:35
 * @Created by yanming.fu
 */
public enum ResponseCode {
    /** 成功 */
    SUCCESS("200", "成功"),

    /** 操作失败 */
    ERROR("500", "操作失败");

    private ResponseCode(String value, String msg){
        this.val = value;
        this.msg = msg;
    }

    public String val() {
        return val;
    }

    public String msg() {
        return msg;
    }

    private String val;
    private String msg;
}
